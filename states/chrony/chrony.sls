{{ tplfile }}> Install {{ slspath }} package:
  pkg.installed:
    - name: {{ slspath }}

{{ tplfile }}> Provide {{ slspath }} ntp sources {{ slspath }}/files/etc/chrony.d:
  file.recurse:
    - name: /etc/chrony.d
    - source: salt://{{ slspath }}/files/etc/chrony.d
    - makedirs: True
    - require:
      - {{ tplfile }}> Install {{ slspath }} package

{{ tplfile }}> Set default ntp server to comment:
  file.replace:
    - name: /etc/chrony.conf
    - pattern: '^(pool.+iburst)'
    - repl: '# \1'
    - bufsize: file
    - require:
      - {{ tplfile }}> Install {{ slspath }} package

{{ tplfile }}> Add include of config directory:
  file.append:
    - name: /etc/{{ slspath }}.conf
    - text: |
        #######################################################################
        ### INCLUDE MORE CONFIGS
        include /etc/{{ slspath }}.d/*.conf
    - require:
      - {{ tplfile }}> Install {{ slspath }} package

# {{ tplfile }}> Configure chrony ntp source:
#   file.replace:
#     - name: /etc/chrony.conf
#     - pattern: '^(pool.+iburst)'
#     - repl: '# \1\n\nserver ntp.metas.ch iburst\n\n! server ntp11.metas.ch iburst\n! server ntp12.metas.ch iburst\n! server ntp13.metas.ch iburst\n! server 0.ch.pool.ntp.org iburst'
#     - bufsize: file
#     - require:
#       - {{ tplfile }}> Install chrony package

{{ tplfile }}> Enable and start {{ slspath }} ntpd service and restart on config changes:
  service.running:
    - name: chronyd
    - enable: True
    - watch:
      - file: /etc/chrony.conf
    - require:
      - {{ tplfile }}> Install {{ slspath }} package


{{ tplfile }}> Stop systemd-timesyncd.service:
  service.dead:
    - name: systemd-timesyncd
    - enable: False
    - require:
      - {{ tplfile }}> Install {{ slspath }} package

{{ tplfile }}> Disable systemd-timesyncd.service:
  service.disabled:
    - name: systemd-timesyncd
    - require:
      - {{ tplfile }}> Stop systemd-timesyncd.service

{{ tplfile }}> mask systemd-timesyncd.service:
  service.masked:
    - name: systemd-timesyncd
    - require:
      - {{ tplfile }}> Disable systemd-timesyncd.service

