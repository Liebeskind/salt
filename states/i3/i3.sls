{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} packages:
  pkg.installed:
    - pkgs:
      - i3-wm
      - i3lock
      - i3status
      - i3blocks

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  file.recurse:
    - name: /home/{{ user }}/.config/{{ slspath }}
    - source: salt://{{ slspath }}/files/home/user/.config/{{ slspath }}
    - makedirs: True
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - {{ tplfile }}> Install {{ slspath }} packages
      - user: {{ user }}

{{ tplfile }}> Configure {{ slspath }} for user root:
  file.recurse:
    - name: /root/.config/{{ slspath }}
    - source: salt://{{ slspath }}/files/root/.config/{{ slspath }}
    - makedirs: True
    - require:
      - {{ tplfile }}> Install {{ slspath }} packages


