{% set user = grains.user %}
# The users to manage on the system. The password hashes can be generated with:
# python3 -c "from getpass import getpass; from crypt import *; p=getpass();print('\n'+crypt(p, METHOD_SHA512)) if p==getpass('Please repeat: ') else print('\nFailed repeating.')"
{{ tplfile }}> Create user {{ user }}:
  user.present:
    - name: {{ user }}
    - fullname: 'Joram'
    - shell: /bin/fish
    - password: '$6$97uZcbUZzes2pdAZ$zU.2A9GlhFzt/jUOPQrenhnxXkKV2ve1NCV5LwTlAwl7wKD7InKnQHBMyo.Ks0BgLq//vTUInP4i./Qmqzt38/'
    - home: /home/{{ user }}
    - groups:
      - wheel
      - storage
      - games
      - power
      - users
      - lp
      - kvm
      - audio
    - require:
      - pkg: sudo

{{ tplfile }}> Add sudoers config for {{ user }}:
  file.managed:
    - name: /etc/sudoers.d/{{ user }}
    - contents: |
        # Host_Alias HOST = {{ grains.host }}
        Cmnd_Alias COMMANDS = /usr/bin/fdisk, /usr/bin/kill, /usr/bin/mount, /usr/bin/umount, /usr/bin/systemctl, /usr/bin/pacman -Suy, /usr/bin/yay -Suy, /usr/bin/mkdir, /usr/bin/updatedb, /usr/bin/vim /etc/fstab, /usr/bin/reboot 0, /usr/bin/shutdown 0
        {{ user }} ALL = (ALL:ALL) NOPASSWD: COMMANDS
        Defaults passwd_timeout=0
        Defaults:{{ user }} !authenticate

