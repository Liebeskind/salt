{% set user = grains.user %}

# https://rasfw-lb-01-v1001.zhaw.ch/CACHE/stc/3/binaries/anyconnect-linux64-4.10.01075-core-vpn-webdeploy-k9.sh

{{ tplfile }}> Install required packages packages:
  pkg.installed:
    - pkgs:
      - gtk3
      - glib2
      - webkit2gtk
#      - gtk2

{{ tplfile }}> Install {{ slspath }} package:
  cmd.run:
    - name: bash anyconnect-linux64-4.10.01075-core-vpn-webdeploy-k9.sh
    - cwd: /home/{{ user }}/data/Software/Cisco
    - unless: test -f /opt/cisco/anyconnect/bin/vpnui
    - require:
      - {{ tplfile }}> Install required packages packages
#      - pkg: openvpn
#      - pkg: openconnect

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  cmd.run:
    - name: cp -p /backup/restore-user/.anyconnect /home/{{ user }}/
    - runas: {{ user }}
    - unless: grep -iq zhaw.ch /home/{{ user }}/.anyconnect
    - require:
      - {{ tplfile }}> Install {{ slspath }} package
      - user: {{ user }}

{{ tplfile }}> Create symlink to anyconnect vpnui:
  file.symlink:
    - name: /usr/local/bin/vpnui
    - target: /opt/cisco/anyconnect/bin/vpnui
    - require:
      - {{ tplfile }}> Install {{ slspath }} package

