# managed by saltstack
base:
  '*':
     - base-devel
     - alacritty
     - dmenu
     - feh
     - firefox
     - fonts
     - htop
     - i3
     - lightdm
     - lightdm-gtk-greeter
     - nvim
     - obsidian
     - pacman-contrib
     - pcmanfm
     - repo
     - ripgrep
     - rsync
     - salt
     - sudo
     - sqlite
     - thunderbird
     - unzip
     - useradd
     - wget
     - xorg
     - yay
     - youtube-dl
     - zathura


#     - repo
#     - updates
#     - locale
#     - network
#     - ssh
#     - timezone
#     - cronie
#     - chrony
#     - rsync
#     - bluetooth
#     - mlocate
#     - base-devel
#     - fonts
#     - perl-modules
#     - audio
#     - bat
#     - pacman-contrib
#     - bash-completion
#     - jq
#     - diff-so-fancy
#     - man-pages
#     - pciutils
#     - usbutils
#     - cups
#     - hplip
#     - lshw
#     - lsof
#     - htop
#     - man-db
#     - p7zip
#     - unzip
#     - wget
#     - xclip
#     - whois
#     - ripgrep
#     - xfsprogs
#     - cifs-utils
#     - dosfstools
#     - pcmanfm
#     - imagemagick
#     - mpv
#     - feh
#     - kate
#     - mupdf
#     - pdfarranger
#     - pdftk
#     - youtube-dl
#     - qpdf
#     - sudo
#     - zsh
#     - mount-backup
#     - mount-data
#     - kitty
#     - vlc
#     - fzf
#     - exa
#     - zprezto
#     - xorg
#     - i3
#     - dmenu
#     - lightdm
#     - keepassxc
#     - strawberry
#     - zathura
#     - nnn
#     - nemo
#     - dolphin
#     - ksnip
#     - nomacs
#     - chromium
#     - firefox
#     - thunderbird
#     - libreoffice
#     - scribus
#     - shotwell
#     - openconnect        # https://wiki.archlinux.org/title/OpenConnect
#     - openvpn
#     - anyconnect
#     - kubectl
#     - k9s
#     - obsidian          # pkg,cfg
#     - yay               # yay
#     - fonts-aur         # yay
#- yay - #    - webex-bin        # yay
#- yay - #     - teamviewer        # yay
#- yay - #     - backintime        # yay
#- yay -      - remmina           # yay
#- yay - #    - logiops           # yay
#- yay -      - qpdfview          # yay


# ssh-add, ssh-agent  Slickedit ENV VAR
#>> #=     - ssh                cfg
#>> #=     - slickedit          cfg
#>> #=     - vpn                cfg
#    - ntfs-3g


# vs i3-wm/*.sls i3lock/*.sls i3status/*.sls perl-anyevent-i3/*.sls i3blocks/*.sls dmenu/*.sls lightdm/*.sls lightdm-gtk-greeter/*.sls

# dir=nnn ; mkdir $dir; echo "include:" >$dir/init.sls; echo "  - .$dir" >>$dir/init.sls; echo "{{ tplfile }}> Install package $dir:">$dir/$dir.sls;echo "  pkg.installed:" >>$dir/$dir.sls;echo "    - pkgs:">>$dir/$dir.sls; echo "      - $dir">>$dir/$dir.sls
# dir=kitty ; mkdir $dir; echo "include:" >$dir/init.sls; echo "  - .$dir" >>$dir/init.sls; echo "{{ tplfile }}> Install package $dir:">$dir/$dir.sls;echo "  pkg.installed:" >>$dir/$dir.sls;echo "    - pkgs:">>$dir/$dir.sls; echo "      - $dir">>$dir/$dir.sls

###=  pacman -Q firefox.desktop
###=  pacman -Q default-web-browser
#=  pacman -Q xdg-settings

