{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} packages:
  pkg.installed:
    - name: {{ slspath }}
    - pkgs:
      - zsh
      - zsh-completions

{{ tplfile }}> Change default shell to {{ slspath }} for user root:
  user.present:
    - name: root
    - shell: /bin/zsh
    - require:
      - {{ tplfile }}> Install {{ slspath }} packages

{{ tplfile }}> Restore .zhistory for user root:
  cmd.run:
    - name: cp -p /backup/restore-root/.zhistory /root/
    - unless: test -f /root/.zhistory
    - onlyif: test -f /backup/restore-root/.zhistory
    - require:
      - {{ tplfile }}> Install {{ slspath }} packages

{{ tplfile }}> Restore .zhistory for user {{ user }}:
  cmd.run:
    - name: cp -p /backup/restore-user/.zhistory /home/{{ user }}/
    - runas: {{ user }}
    - unless: test -f /home/{{ user }}/.zhistory
    - onlyif: test -d /home/{{ user }} && test -f /backup/restore-user/.zhistory
    - require:
      - {{ tplfile }}> Install {{ slspath }} packages

        # password is `arch`
        # password: '$6$uygmzqWkzVtkoSTG$yQpQ3.WaB92p.TPZf0LBvQJDblnbPKlM94bcEQ0uAS.5XMMf4EbrG7v9fdY3CRIyDpmiWwrmwHvvIXvoPUu9u/'
#    - name: uri
#        shell: /usr/bin/zsh
#        # password is `arch`
#        groups:
#          - wheel
#          - uucp
#          - video
#          - audio
#          - storage
#          - games
#          - input
#        files:
#          - name: .config/i3/config
#            # source can be hosted on any http, ftp server or inside the state directory (salt://)
#            source: https://raw.githubusercontent.com/mdaffin/dotfiles/master/i3.config
#            # source_hash is optional, but ensure that the config will not be changed without you approving it by updating the hash
#            source_hash: 3322b5826ce2661f498b5e31ec83177764fe314abb60577bb143c2e5aeec9b76a16781b65d7387251803c81a034bf443c848f97a77584516beb488adb21b86d3


