{% set user = grains.user %}

{{ tplfile }}> Mount backup volume:
  mount.mounted:
    - name: /backup
    - device: UUID=f2377af8-638d-4794-a039-544778f5cef2
    - fstype: xfs
    - opts: rw,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota
    - dump: 0
    - pass_num: 0
    - persist: True
    - mkmnt: True


