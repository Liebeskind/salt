{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
  pkg.installed:
    - name: {{ slspath }}

# {{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
#   file.recurse:
#     - name: /home/{{ user }}/.config/{{ slspath }}rc
#     - source: salt://{{ slspath }}/files/home/user/.config/{{ slspath }}rc
#     - makedirs: True
#     - user: {{ user }}
#     - group: {{ user }}
#     - require:
#       - {{ tplfile }}> Install {{ slspath }} package
#       - user: {{ user }}

# {{ tplfile }}> Configure {{ slspath }} for user root:
#   file.recurse:
#     - name: /root/.config/{{ slspath }}rc
#     - source: salt://{{ slspath }}/files/root/.config/{{ slspath }}rc
#     - makedirs: True
#     - require:
#       - {{ tplfile }}> Install {{ slspath }} package


#+ {{ tplfile }}> Configuing {{ slspath }} for user {{ user }}:

#+   cmd.run:
#+     - name: cp /backup/restore-user/.config/dolphinrc /home/{{ user }}/.config/
#+     - unless: test -f /home/{{ user }}/.config/dolphinrc
#+     - require:
#+       - {{ tplfile }}> Install {{ slspath }} package

#+ {{ tplfile }}> Restore {{ slspath }} data for user {{ user }}:
#+   cmd.run:
#+     - name: rsync -a /backup/restore-user/.local/share/{{ slspath }}
#+     - onlyif: test -d /backup/restore-user/.local/share/{{ slspath }}
#+     - unless: test -d /home/{{ user }}/.local/share/{{ slspath }}
#+     - require:
#+       - {{ tplfile }}> Install {{ slspath }} package


#+ {{ tplfile }}> Configuing {{ slspath }} for user root:
#+   cmd.run:
#+     - name: cp /backup/restore-user/.config/dolphinrc /root/.config/
#+     - unless: test -f /root/.config/dolphinrc
#+     - require:
#+       - {{ tplfile }}> Install {{ slspath }} package
#+


#  file.managed:
#    - name: /root/.config/dolphinrc
#    - source: salt://{{ slspath }}/files/.config/dolphinrc
#    - require:
#      - {{ tplfile }}> Install dolphin package
