{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
  pkg.installed:
    - name: {{ slspath }}

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  file.managed:
    - name: /home/{{ user }}/.fzf.zsh
    - source: salt://{{ slspath }}/files/home/user/.fzf.zsh
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - {{ tplfile }}> Install {{ slspath }} package
      - pkg: zsh
      - user: {{ user }}

{{ tplfile }}> Configure {{ slspath }} for user root:
  file.managed:
    - name: /root/.fzf.zsh
    - source: salt://{{ slspath }}/files/root/.fzf.zsh
    - require:
        - {{ tplfile }}> Install fzf package
        - pkg: zsh

