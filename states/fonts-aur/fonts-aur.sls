{% import_yaml slspath ~ "/fonts-aur.yaml" as fonts_aur %}

{% for font, font_specs in fonts_aur.fonts.items()      %}
{% set test = font_specs.test                           %}
{{ tplfile }}> Install font {{ font }}:
  cmd.run:
    - name: yay -S {{ font }} --answerclean None --answerdiff None --answeredit None
    - unless: test {{ test }}
{% endfor %}


