{{ tplfile }}> Install base-devel packages:
  pkg.installed:
    - pkgs:
      - autoconf
      - automake
      - binutils
      - bison
      - fakeroot
      - file
      - findutils
      - flex
      - gawk
      - gcc
      - gettext
      - grep
      - gzip
      - libtool
      - m4
      - make
      - pacman
      - patch
      - pkgconf
      - sed
      - sudo
      - texinfo
      - which

