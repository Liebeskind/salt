{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
  cmd.run:
    - name: yay -S {{ slspath }} --answerclean None --answerdiff None --answeredit None
    - runas: {{ user }}
    - unless: test -f /usr/bin/{{ slspath }}

# backintime-qt
{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  file.managed:
    - name: /home/{{ user }}/.config/{{ slspath }}/config
    - source: salt://{{ slspath }}/files/home/user/.config/{{ slspath }}/config
    - makedirs: True
    - user: {{ user }}
    - group: {{ user }}
    - unless: grep -q "MANAGED BY SLATSTACK" /home/{{ user }}/.config/{{ slspath }}/config
    - require:
      - {{ tplfile }}> Install {{ slspath }} package
      - user: {{ user }}

{{ tplfile }}> Schedule 1-st backup for user {{ user }}:
  cron.present:
    - name: /usr/bin/nice -n19 /usr/bin/ionice -c2 -n7 /usr/bin/backintime backup-job >/dev/null
    - user: {{ user }}
    - minute: 0

{{ tplfile }}> Schedule 2-nd backup for user {{ user }}:
  cron.present:
    - name: /usr/bin/nice -n19 /usr/bin/ionice -c2 -n7 /usr/bin/backintime --profile-id 2 backup-job >/dev/null
    - user: {{ user }}
    - minute: 15

{{ tplfile }}> Delete /tmp/backintime.lock-File for user {{ user }}:
  cron.present:
    - name: rm /tmp/backintime.lock >/dev/null
    - user: {{ user }}
    - minute: 30

# sudo pkexec backintime-qt
{{ tplfile }}> Configure {{ slspath }} for user root:
  file.managed:
    - name: /root/.config/{{ slspath }}/config
    - source: salt://{{ slspath }}/files/root/.config/{{ slspath }}/config
    - makedirs: True
    - unless: grep -q "MANAGED BY SLATSTACK" /root/.config/{{ slspath }}/config
    - require:
      - {{ tplfile }}> Install {{ slspath }} package

{{ tplfile }}> Schedule 1-st backup crontab for user root:
  cron.present:
    - name: /usr/sbin/nice -n19 /usr/sbin/ionice -c2 -n7 /usr/sbin/backintime backup-job >/dev/null
    - user: root
    - minute: 35

{{ tplfile }}> Schedule 2-nd backup crontab for user root:
  cron.present:
    - name: /usr/sbin/nice -n19 /usr/sbin/ionice -c2 -n7 /usr/sbin/backintime --profile-id 2 backup-job >/dev/null
    - user: root
    - minute: 45

{{ tplfile }}> Delete /tmp/backintime.lock-File for user root:
  cron.present:
    - name: rm /tmp/backintime.lock >/dev/null
    - user: root
    - minute: 30


