# STARTX WITH XTERM
# startx

# KILL XTERM
# Ctrl+Alt+F2
# sudo kill xterm

{{ tplfile }}> Installing xorg packages:
  pkg.installed:
    - pkgs:
      - xorg-server
      - xorg-xsetroot
      - xorg-xkill
      - xorg-xbacklight
      - xf86-input-synaptics
      - xf86-input-libinput
      - xorg-xdpyinfo
      - xorg-xhost
      - xorg-xinit
      - numlockx
      - xterm
      - arandr
#      - xorg-xsetroot


{{ tplfile }}> Installing xorg video adapter card packages:
  pkg.installed:
    - pkgs:
{% for gpu in grains.gpus        %}
{%   if gpu.vendor == 'intel'    %}
      - xf86-video-intel
{%   elif gpu.vendor == 'nvidia' %}
      - xf86-video-intel
      - nvidia
{%   else                        %}
      - xf86-video-vesa
{%   endif                       %}
{% endfor                        %}


{{ tplfile }}> Set mouse cursor:
  file.replace:
    - name: /usr/share/icons/default/index.theme
    - pattern: "^Inherits=Adwaita"
    - backup: False
    - repl: "# Inherits=Adwaita"
    - bufsize: file
    - require:
      - {{ tplfile }}> Installing xorg packages

