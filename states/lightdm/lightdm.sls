{{ tplfile }}> Install {{ slspath }} package:
  pkg.installed:
    - pkgs:
      - lightdm
      - lightdm-gtk-greeter

# https://wiki.archlinux.org/title/Activating_numlock_on_bootup
{{ tplfile }}> Turn on numlock:
  file.replace:
    - name: /etc/lightdm/lightdm.conf
    - pattern: "^#greeter-setup-script="
    - backup: False
    - repl: "greeter-setup-script=/usr/bin/numlockx on"
    - bufsize: file
    - require:
      - {{ tplfile }}> Install {{ slspath }} package
#      - {{ tplfile }}> Installing xorg packages

{{ tplfile }}> Enable lightdm and restart if config has changed:
  service.running:
    - name: lightdm
    - enable: True
    - require:
      - {{ tplfile }}> Install {{ slspath }} package
#    - watch:
#       - file: /etc/ssh/sshd_config

