{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
  pkg.installed:
    - name: {{ slspath }}

{{ tplfile }}> Configure {{ slspath }} for user {{ user }} FROM {{ slspath }}/files/home/user/.config/{{ slspath }}:
  file.recurse:
    - name: /home/{{ user }}/.config/{{ slspath }}
    - source: salt://{{ slspath }}/files/home/user/.config/{{ slspath }}
    - makedirs: True
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - {{ tplfile }}> Install {{ slspath }} package
      - user: {{ user }}

{{ tplfile }}> Configure {{ slspath }} for user root:
  file.recurse:
    - name: /root/.config/{{ slspath }}
    - source: salt://{{ slspath }}/files/root/.config/{{ slspath }}
    - makedirs: True
    - require:
      - {{ tplfile }}> Install {{ slspath }} package

#{{ tplfile }}> Restore {{ slspath }} data for user {{ user }}:
#  cmd.run:
#    - name: rsync -a /backup/restore-user/Documents/Keepass /home/{{ user }}/Documents/
#    - runsas: {{ user }}
#    - unless: test -d /home/{{ user }}/Documents/
#    - require:
#      - {{ tplfile }}> Install {{ slspath }} package

#  file.recurse:
#    - name: /root/.config/keepassxc
#    - source: salt://{{ slspath }}/files/.config/keepassxc
#    - makedirs: True
#    - require:
#      - {{ tplfile }}> Install keepassxc package


