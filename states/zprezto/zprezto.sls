{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} for user {{ user }}:
  file.recurse:
    - name: /home/{{ user }}/.{{ slspath }}
    - source: salt://{{ slspath }}/files/home/user/.{{ slspath }}
    - makedirs: True
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - pkg: zsh

# {{ tplfile }}> Configure {{ slspath }} prompt config for user {{ user }}:
#   file.replace:
#     - name: /home/{{ user }}/.zprezto/runcoms/zpreztorc
#     - runas: {{ user }}
#     - backup: False
#     - pattern: "^zstyle ':prezto:module:prompt' theme .+"
#     - repl: "zstyle ':prezto:module:prompt' theme 'skwp'"
#     - bufsize: file
#     - require:
#       - {{ tplfile }}> Install {{ slspath }} for user {{ user }}

{% for cfgfile in salt.file.find('/home/' ~ user ~ '/.zprezto/runcoms/', type='f', name='z*', maxdepth=1) %}

{% set cfgfile_name = salt.file.basename(cfgfile)                                                      %}
{{ tplfile }}> Create config file symlink cfgfile_name {{ cfgfile }} for user {{ user }}:
  file.symlink:
    - name: /home/{{ user }}/.{{ cfgfile_name }}
    - target: {{ cfgfile }}
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - {{ tplfile }}> Install {{ slspath }} for user {{ user }}
{% endfor %}


{{ tplfile }}> Install {{ slspath }} for user root:
  file.recurse:
    - name: /root/.{{ slspath }}
    - source: salt://{{ slspath }}/files/home/user/.{{ slspath }}
    - makedirs: True
    - require:
      - pkg: zsh

{% for cfgfile in salt.file.find('/root/.zprezto/runcoms', type='f', name='z*', maxdepth=1) %}
{% set cfgfile_name = salt.file.basename(cfgfile)                                           %}
{{ tplfile }}> Create config file symlink cfgfile_name {{ cfgfile }} for user root:
  file.symlink:
    - name: /root/.{{ cfgfile_name }}
    - target: {{ cfgfile }}
    - require:
      - {{ tplfile }}> Install {{ slspath }} for user root
{% endfor %}


# {{ tplfile }}> Configure {{ slspath }} prompt config for user root:
#   file.replace:
#     - name: /root/.zprezto/runcoms/zpreztorc
#     - pattern: "^zstyle ':prezto:module:prompt' theme .+"
#     - backup: False
#     - repl: "zstyle ':prezto:module:prompt' theme 'skwp'"
#     - bufsize: file
#     - require:
#       - {{ tplfile }}> Install {{ slspath }} for user root


#{{ tplfile }}> Clone {{ slspath }} from git for user {{ user }}:
#    git.cloned:
#      - name: git@github.com:sorin-ionescu/prezto.git
#      - target: /home/{{ user }}/.zprezto
#      - runas: {{ user }}
#      - branch: master
#      - require:
#         - pkg: zsh
#      #- force_clone: true

# {{ tplfile }}> Configure {{ slspath }} prompt config for user {{ user }}:
#   file.replace:
#     - name: /home/{{ user }}/.zprezto/runcoms/zpreztorc
#     - runas: {{ user }}
#     - backup: False
#     - pattern: "^zstyle ':prezto:module:prompt' theme .+"
#     - repl: "zstyle ':prezto:module:prompt' theme 'skwp'"
#     - bufsize: file
#     - require:
#       - {{ tplfile }}> Clone {{ slspath }} from git for user {{ user }}

