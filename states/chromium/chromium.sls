{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
  pkg.installed:
    - name: {{ slspath }}

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  cmd.run:
    - name: rsync -a /backup/restore-user/.config/{{ slspath }} /home/{{ user }}/.config/
    - runas: {{ user }}
    - unless: test -d /home/{{ user }}/.config/{{ slspath }}
    - require:
      - {{ tplfile }}> Install {{ slspath }} package

#+ {{ tplfile }}> Configure chromium for user root:
#+   cmd.run:
#+     - name: rsync -a /backup/restore-user/.config/{{ slspath }} /root/.config/
#+     - unless: test -d /root/.config/{{ slspath }}
#+     - require:
#+       - {{ tplfile }}> Install {{ slspath }} package
