export TERM=linux

# ADD SUDO ACCESS TO THE USER
ln -s /usr/bin/vim /usr/bin/vi

# SET SUDOERS
sudo cp /backup/backintime/$(hostnamectl hostname)/$USER/data/restore-snapshot/backup/etc/sudoers.d/$USER /etc/sudoers.d/

/etc/sudoers.d/luri
Host_Alias HOST = clt-dsk-v-2008
Cmnd_Alias COMMANDS = /usr/bin/fdisk, /usr/bin/kill, /usr/bin/mount, /usr/bin/umount, /usr/bin/systemctl, /usr/bin/pacman, /usr/bin/mkdir, /usr/bin/updatedb
luri HOST=(root) NOPASSWD: COMMANDS

# LOGIN AS USER
su - luri
mkdir Desktop Documents Music Downloads Pictures Special git

# USERS BIN
cp -r /backup/backintime/$(hostname)/$USER/data/restore-snapshot/backup/home/$USER/bin /home/$USER/

# SSH CONFIG
ssh-keygen
cp -r /backup/backintime/$(hostname)/$USER/data/restore-snapshot/backup/home/$USER/.ssh/* /home/$USER/.ssh/
ssh


--------------------------------------------------------------------------
INSTALL WPA_SUPPLICANT
---------------------------------------------------------------------------
sudo pacman -S wpa_supplicant


---------------------------------------------------------------------------
CISCO ANYCONNECT
---------------------------------------------------------------------------
1. Get software: https://ras.zhaw.ch
2. Copy config: cp /backup/...   /opt/cisco/anyconnect/profile/

cd /backup/Software/Cisco/
gunzip anyconnect-linux64-4.7.04056-predeploy-k9.tar.gz
cd anyconnect...
sudo ./vpn_install.sh
/opt/cisco/anyconnect/bin/vpn -h
/opt/cisco/anyconnect/bin/vpn connect ras.zhaw.ch

# CREATE LINK
sudo ln -s /opt/cisco/anyconnect/bin/vpn /usr/local/bin/vpn

# COPY PROFILE
sudo cp -r /backup/backintime/$(hostname)/$USER/data/restore-snapshot/backup/opt/cisco/profile      /opt/cisco/
sudo cp -r /backup/backintime/$(hostname)/$USER/data/restore-snapshot/backup/home/$USER/.anyconnect /home/$USER/


# ZHAW SHARE PATHS
sudo mkdir -p /{u,s,p,x,r}

# SLICKEDIT
install slickedit
cd /backup/software/slickedit
tar xzf se_2400...tar.gz
cd se_2400...
sudo ./vsinst
PATH: /opt/slickedit/
ln -s /opt/slickedit/bin/vs /usr/local/bin/vs


---------------------------------------------------------------------------
WPA SUPPLICANT 802.1x
---------------------------------------------------------------------------
https://wiki.archlinux.org/index.php/Wpa_supplicant#802.1x/radius

### 802.1x
sudo pacman -S wpa_supplicant

/etc/wpa_supplicant/wpa_supplicant-wired-eno1.conf
key_mgmt=IEEE8021X
eap=PEAP
identity="luri@zhaw.ch"
password="asd fadsfa"
phase2="autheap=MSCHAPV2"

sudo chown root: /etc/wpa_supplicant/wpa_supplicant-wired-eno1.conf
sudo chmod 600 /etc/wpa_supplicant/wpa_supplicant-wired-eno1.conf

sudo ip link set eno1 down

systemctl start wpa_supplicant-wired@eno1.service
systemctl enable wpa_supplicant-wired@eno1.service


# ZHAW SHARE PATHS
sudo mkdir -p /{c,u,s,p,x,r}
sudo mount -t cifs //user.zhaw.ch/staff        /u -o username=luri@zhaw.ch
sudo mount -t cifs //clt-dsk-v-2008.zhaw.ch/c$ /c -o username=luri@zhaw.ch

as uri:
sudo mount -t cifs //user.zhaw.ch/staff /u -o credentials=/etc/cred

username=luri
password=...
domain=zhaw.ch


# INSTALL SLICKEDIT
cd /backup/software/slickedit
tar xzf se_2400...tar.gz
cd se_2400...
sudo ./vsinst
PATH: /opt/slickedit/
ln -s /opt/slickedit/bin/vs /usr/local/bin/vs



---------------------------------------------------------------------------
GIT REPOS
---------------------------------------------------------------------------
cd git
git clone git@git.zhaw.ch:ontap
git clone git@git.zhaw.ch:tmsh
git clone git@git.zhaw.ch:san
git clone git@git.zhaw.ch:zreg
git clone git@git.zhaw.ch:fax
git clone git@git.zhaw.ch:salt/pillars salt/pillars
git clone git@git.zhaw.ch:salt salt/states
git clone git@git.zhaw.ch:gitolite-admin



---------------------------------------------------------------------------
SSH DISABLE
---------------------------------------------------------------------------
sudo systemctl stop sshd
sudo systemctl disable sshd


---------------------------------------------------------------------------
MORE I3
---------------------------------------------------------------------------
numlock i3
prompt zsh

elinks

# PROMPT='%F{cyan}%n%f@%m:%d ${git_info:+${(e)git_info[prompt]}}${editor_info[keymap]} '



---------------------------------------------------------------------------
VIRTUALBOX
---------------------------------------------------------------------------
sudo pacman -S virtualbox
sudo pacman -S virtualbox-guest-iso

# MOUNT /dev/nvme1n1p5 /vm
# Settings virtualbox path: /vm/VirtualBox
sudo usermod -aG vboxusers uri


# Extension pack for USB3 support
yay -S virtualbox-ext-oracle



---------------------------------------------------------------------------
HELP / INFOS
---------------------------------------------------------------------------
# MENU ICONS
https://fontawesome.com/v4.7.0/cheatsheet/

#configure i3 bar with i3blocks
https://www.youtube.com/watch?v=ARKIwOlazKI?t=38s

# windows and bar colors explained
https://www.youtube.com/watch?v=ARKIwOlazKIji

# choosing colors...
https://github.com/bookercodes/dotfiles

# install and customize file explorer tunar and customize gtk theme:
https://www.youtube.com/watch?v=ARKIwOlazKI?t=20m0s

# install and customize rofi
https://www.youtube.com/watch?v=ARKIwOlazKI?t=28m0s


sudo pacman -S imagemagick


rename i3 workspaces...

start certain program in certain workspace
https://www.youtube.com/watch?v=8-S0cWnLBKg&t=24mr20s
get window class:
xprop
click on application
get second value of WM_CLASS Back In Time

assign icon to dmenu entry
goto font awesome cheatsheet
https://fontawesome.com/v4.7.0/cheatsheet/
copy the icon and insert it in the


# set new i3 font:
yay -S system-san-francisco-font-git
.config/i3/config
font pango:System San Francisco Display 10



-------------------------------------------------------------------------
# CHECK LATER
-------------------------------------------------------------------------
#ranger
#rofi
#conky
pygmentize
keychain
python-pdftotext

#sudo pacman -S
atool
highlight
browsh
elinks
mediainfo
ffmpegthumbnailer

install arandr for xrandr as graphical interface for xcreen controll
can generate xrandr commands for example to change resolution.
this may be added to i3/config
exec_allways xrandr....

# choosing colors...
https://github.com/bookercodes/dotfiles

# windows and bar colors explained
https://www.youtube.com/watch?v=ARKIwOlazKIji

# install and customize file explorer tunar and customize gtk theme:
https://www.youtube.com/watch?v=ARKIwOlazKI?t=20m0s

# install and customize rofi
https://www.youtube.com/watch?v=ARKIwOlazKI?t=28m0s

#install imagemackick
sudo pacman -S imagemagick

#configure i3 bar with i3blocks
https://www.youtube.com/watch?v=ARKIwOlazKI?t=38s

#

.bashrc:
# Let vom preservre environment such as .vimrc
alias vim='vim -E'

# choosing colors...
https://github.com/bookercodes/dotfiles

# windows and bar colors explained
https://www.youtube.com/watch?v=ARKIwOlazKIji

# install and customize file explorer tunar and customize gtk theme:
https://www.youtube.com/watch?v=ARKIwOlazKI?t=20m0s

# install and customize rofi
https://www.youtube.com/watch?v=ARKIwOlazKI?t=28m0s

#install imagemackick
sudo pacman -S imagemagick

#configure i3 bar with i3blocks
https://www.youtube.com/watch?v=ARKIwOlazKI?t=38s


.bashrc:
# Let vom preservre environment such as .vimrc
alias vim='vim -E'




-------------------------------------------------------------------------
MORE
-------------------------------------------------------------------------
start certain program in certain workspace
https://www.youtube.com/watch?v=8-S0cWnLBKg&t=24mr20s
get window class:
xprop
click on application
get second value of WM_CLASS Back In Time

assign icon to dmenu entry
goto font awesome cheatsheet
https://fontawesome.com/v4.7.0/cheatsheet/
copy the icon and insert it in the


source .zshrc

plsorph
pclean
paurlist

vim zlog
cd .zprezto/runcoms
vimdiff zprofile /home/uri/.zprofile
...
cd Desktop/config
vim .zhistory

cat .Xresources
vim .zshrc

pacman -R mtp jmtpfs
pacman -R jmtpfs

prompt agnoster
prompt cloud
prompt damoekri
prompt giddie
prompt kylewest
prompt minimal
prompt nicoulaj
prompt paradox
prompt peepcode
prompt powerlevel10k
prompt powerline
prompt sorin
prompt pur
prompt pure
prompt skwp
prompt smiley
prompt steeef
prompt adam1
prompt adam2
prompt bart
prompt bigfade
prompt clint
prompt default
prompt elite2
prompt elite
prompt fade
prompt fire
prompt off
prompt oliver
prompt pws
prompt redhat
prompt restore
prompt walters
prompt zefram
prompt suse

vim .config/i3/config
locate arch-linux-3840x2160.png
cp /home/uri/Pictures/wallpapers/arch-linux-3840x2160.png /home/uri/.config/i3
logout
reboot 0
sudo reboot 0
lightdm-gtk-greeter

sudo vim /etc/lightdm/lightdm-gtk-greeter.conf
ls /home/uri/.config/i3/arch-linux-3840x2160.jpg
sudo cp /home/uri/.config/i3/arch-linux-3840x2160.jpg /usr/share/pixmaps/
ls /usr/share/pixmaps
sudo cp /home/uri/.config/i3/arch-linux-3840x2160.png /usr/share/pixmaps/
vim /etc/lightdm/lightdm-gtk-greeter.conf

light-locker-command -l
man lightdm-gtk-greeter
lightdm-gtk-greeter --help
sudo vim /etc/sudoers.d/uri
sudo visudo /etc/sudoers.d/uri

sudo visudo /etc/sudoers.d/uri




