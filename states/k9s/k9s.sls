{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
  pkg.installed:
    - name: {{ slspath }}

# {{ tplfile }}> Create {{ slspath }} config directory for user {{ user }}:
#   file.directory:
#     - name: /home/{{ user }}/.kube
#     - user: {{ user }}
#     - group: {{ user }}
#     - unless: test -d /home/{{ user }}/.kube
#     - require:
#       - {{ tplfile }}> Install {{ slspath }} packages
#
# {{ tplfile }}> Restore {{ slspath }} config for user {{ user }}:
#   cmd.run:
#     - name: cp -p /backup/restore-user/.kube /home/{{ user }}/
#     - runas: {{ user }}
#     - unless: test -f /home/{{ user }}/.zhistory
#     - onlyif: test -d /home/{{ user }} && test -f /backup/restore-user/.zhistory
#     - require:
#       - {{ tplfile }}> Create {{ slspath }} config directory for user {{ user }}

