-- Packer boostrap
local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
    packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end


-- Auto compile when plugins.lua is changed
vim.cmd([[
    augroup packe_user_config
        autocmd!
        autocmd BufWritePost plugins.lua source <afile> | PackerCompile
    augroup end
]])

return require('packer').startup(function(use)
    use 'wbthomason/packer.nvim'

    use {'tpope/vim-dispatch', opt=true, cmd={'Dispatch', 'Make', 'Focus', 'Start'}}

    use 'williamboman/mason.nvim'
    use 'williamboman/mason-lspconfig.nvim'
    use 'neovim/nvim-lspconfig'

    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-path'
    use 'hrsh7th/cmp-cmdline'
    use 'hrsh7th/cmp-omni'
    use 'hrsh7th/nvim-cmp'

    use 'L3MON4D3/LuaSnip'
    use 'saadparwaiz1/cmp_luasnip'

    use 'nvim-lua/plenary.nvim'
    use 'nvim-lua/popup.nvim'

    use {
        'nvim-treesitter/nvim-treesitter',
        run = function () require('nvim-treesitter.install').update({ with_sync = true }) end,
    }

    use {
        "numToStr/Comment.nvim",
        event = "BufRead",
        config = function()
            require('Comment').setup{}
        end
    }

    use {
        'folke/which-key.nvim',
        config = function()
            require('which-key').setup({
                window = {
                    border = "shadow"
                },
                triggers = "auto"
            })
        end
    }

    use { 'lervag/vimtex' }

    use { 'EdenEast/nightfox.nvim' }

    use {
        'kyazdani42/nvim-tree.lua',
        requires = 'kyazdani42/nvim-web-devicons'
    }

    use 'tpope/vim-fugitive'
    use 'tpope/vim-surround'

    if packer_bootstrap then
        require('packer').sync()
    end
end)
