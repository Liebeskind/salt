-- require'lspconfig'.texlab.setup{
--     cmd = { "/home/joram/.local/share/nvim/lspinstall/latex/texlab" }
-- }

local function ltex_setup()
    require'lspconfig'.ltex.setup{
        settings = {
            ltex = {
                latex = {
                    commands = {
                        ["\\label{}"] = "ignore",
                        ["\\todo{}"] = "ignore",
                        ["\\vref{}"] = "ignore",
                    },
                },
                dictionary = {
                    ["en-US"] = {
                        "MVAE",
                        "MVAEs",
                        "VAE",
                        "TC-VAE",
                        "TC-MVAE",
                        "ELBO",
                        "entropy",
                        "entropies",
                        "dSprites",
                        "MdSprites",
                        "PolyMNIST",
                        "MNIST",
                        "latent",
                        "latents",
                        "Minibatch",
                    },
                },
            },
        },
    }
end

ltex_setup()

vim.keymap.set('n', '<leader><leader>r', function ()
    require'mathtex-overlay'.destroy()
    package.loaded['mathtex-overlay'] = nil
    require'mathtex-overlay'.setup()
end)


vim.keymap.set('n', '<leader><leader>d', ltex_setup)
