require('plugins')
require('lsp')
require('keymappings')
require('snip')

------------------
-- Colorscheme ---
------------------
-- require('nightfox').load('nordfox')
vim.cmd("colorscheme dayfox")

--------------------------------------------
-- mason.nvim (External tooling like LSP) --
--------------------------------------------
mason = require("mason").setup()

require("mason-lspconfig").setup({
    ensure_installed = {
        "sumneko_lua",
        "ltex",
        "pylsp",
        "clangd",
    }
})



-------------------
-- Misc Settings --
-------------------
vim.opt.backup = true
vim.opt.clipboard = "unnamedplus"
vim.opt.cmdheight = 2
vim.opt.completeopt = { "menuone", "noselect" }
vim.opt.conceallevel = 0
vim.opt.fileencoding = "utf-8"
vim.opt.foldmethod = "manual"
vim.opt.foldexpr = ""
vim.opt.hidden = true
vim.opt.list = true
vim.opt.listchars = "eol:¬,tab:» ,trail:☠"
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.tabstop = 4
vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 0
vim.opt.cursorline = true


vim.opt.backupdir="/home/joram/.local/cache/nvim/backup/"
vim.opt.backup = true

-------------
-- VimWiki --
-------------

vim.g.wiki_root = "~/Documents/ObsidianZK/"
vim.g.wiki_zotero_root = "~/.local/share/Zotero/"
vim.g.wiki_filetypes = {'md'}
vim.g.wiki_link_extension = 'md'
vim.g.wiki_link_target_type = 'md'

vim.g.maplocalleader = ' '
vim.g.mapleader = ' '

------------
-- VimTeX --
------------
vim.g.tex_stylish = 1
vim.g.tex_flavor = 'latex'

vim.g.vimtex_fold_enabled = 0
vim.g.vimtex_format_enabled = 1
vim.g.vimtex_view_method = 'zathura'
vim.g.vimtex_view_general_viewer = 'zathura'
vim.g.vimtex_view_zathura_check_libsynctex = 1
vim.g.vimtex_view_automatic = 0
vim.g.vimtex_view_forward_search_on_start = 0
vim.g.vimtex_toc_config = {
    split_pos = 'vert leftabove',
    mode = 2,
    fold_enable = 0,
    hotkeys_enabled = 1,
    hotkeys_leader = '',
    refresh_always = 0
}

vim.g.vimtex_quickfix_open_on_warning = 0
vim.g.vimtex_quickfix_autoclose_after_keystrokes = 3
vim.g.vimtex_imaps_enabled = 1
vim.g.vimtex_complete_img_use_tail = 1
vim.g.vimtex_complete_bib = {
    simple = 1,
    menu_fmt = '@year, @author_short, @title',
}

vim.g.vimtex_echo_verbose_input = 0

vim.g.vimtex_compiler_progname = '/usr/bin/nvr'

vim.g.vimtex_compiler_method = 'latexmk'
vim.g.vimtex_compiler_latexmk = {
    background = 1,
    build_dir = 'build/',
    callback = 1,
    continuous = 1,
    executable = 'latexmk',
    hooks = {},
    options = {
        '-pdf',
        '-verbose',
        '-file-line-error',
        '-synctex=1',
        '-interaction=nonstopmode'
    }
}

vim.g.vimtex_compile_latexmk_engines = {
    ["pdftex"] = "-pdf -pdflatex=pdftex"
}


vim.g.completeopt = { "menu", "menuone", "noselect" }
-- vim.g.timeoutlen = 100
-- vim.g.updatetime = 300
--
vim.g.updatetime = 300


-- nvim-tree

require'nvim-tree'.setup{}

local wk = require'which-key'
wk.register({
    ["<leader>"] = {
        n = { "<cmd>NvimTreeToggle<CR>", "Nvim Tree Toggle" }
    }
})


---------------
-- nvim-tree --
---------------
require'nvim-tree'.setup {}




---------
-- cmp --
---------
local luasnip = require("luasnip")
local cmp = require'cmp'
local has_words_before = function()
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

cmp.setup({
    snippet = {
        expand = function (args)
            require'luasnip'.lsp_expand(args.body)
        end,
    },
    window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
        ['<C-b>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-e>'] = cmp.mapping.abort(),
        ['<CR>'] = cmp.mapping.confirm({ select = true }),
        ['<Tab>'] = cmp.mapping(function(fallback)
            if luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            elseif cmp.visible() then
                cmp.select_next_item()
            elseif has_words_before() then
                cmp.complete()
            else
                fallback()
            end
        end, { "i", "s" }),
    }),
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        { name = 'luasnip' },
        { name = 'omni' },
        { name = 'buffer' },
    }, {}
    ),
})

cmp.setup.buffer {
    formatting = {
        format = function(entry, vim_item)
            vim_item.menu = ({
                omni = (vim.inspect(vim_item.menu):gsub('%"', "")),
                luasnip = "[LuaSnip]",
                nvim_lsp = "[LSP]",
                buffer = "[Buffer]",
            })[entry.source.name]
            return vim_item
        end,
    },
    sources = {
        { name = 'nvim_lsp' },
        { name = 'omni' },
        { name = 'buffer' },
    },
}




