-- require'lspconfig'.pyright.setup{
--     cmd = { vim.fn.stdpath('data') .. '/lsp_servers/python/node_modules/.bin/pyright-langserver', '--stdio' }
-- }
require'lspconfig'.pylsp.setup{
    on_attach = function(client)
        vim.api.nvim_buf_set_keymap(0, 'n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', {noremap = true})
    end,
    capabilities = require('cmp_nvim_lsp').default_capabilities()
}

