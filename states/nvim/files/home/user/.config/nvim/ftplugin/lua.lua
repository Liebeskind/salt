-- local custom_lsp_attach = function(client)
-- -- See `:help nvim_buf_set_keymap()` for more information
-- vim.api.nvim_buf_set_keymap(0, 'n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', {noremap = true})
-- vim.api.nvim_buf_set_keymap(0, 'n', '<c-]>', '<cmd>lua vim.lsp.buf.definition()<CR>', {noremap = true})
-- -- ... and other keymappings for LSP
-- 
-- -- Use LSP as the handler for omnifunc.
-- --    See `:help omnifunc` and `:help ins-completion` for more information.
-- vim.api.nvim_buf_set_option(0, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
-- 
-- -- For plugins with an `on_attach` callback, call them here. For example:
-- -- require('completion').on_attach()
-- end
-- 
-- 
-- -- An example of configuring for `sumneko_lua`,
-- --  a language server for Lua.
-- 
-- -- set the path to the sumneko installation
-- -- local sumneko_root_path = vim.fn.stdpath('data')..'/lsp_servers/sumneko_lua/extension/server'
-- --
-- local sumneko_root_path = vim.fn.stdpath('data') .. '/mason/bin/lua-language-server'
-- 
-- local sumneko_binary = sumneko_root_path.."/bin/lua-language-server"
 
local custom_lsp_attach = function(client)
    vim.api.nvim_buf_set_keymap(0, 'n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', {noremap = true})
    vim.api.nvim_buf_set_keymap(0, 'n', '<c-]>', '<cmd>lua vim.lsp.buf.definition()<CR>', {noremap = true})
    vim.api.nvim_buf_set_option(0, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
end

local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")
table.insert(runtime_path, "ftplugin/?.lua")
table.insert(runtime_path, "ftplugin/?/init.lua")
table.insert(runtime_path, "init.lua")

require'lspconfig'.sumneko_lua.setup {
    settings = {
        Lua = {
            runtime = {
                -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                version = 'LuaJIT',
                -- Setup your lua path
                -- path = runtime_path,
            },
            diagnostics = {
                -- Get the language server to recognize the `vim` global
                globals = {'vim'},
            },
            workspace = {
                -- Make the server aware of Neovim runtime files
                library = vim.api.nvim_get_runtime_file("", true),
            },
            -- Do not send telemetry data containing a randomized but unique
            telemetry = {
                enable = false,
            },
        },
    },
    on_attach = custom_lsp_attach,
    capabilities = require('cmp_nvim_lsp').default_capabilities()
}



