local wk = require'which-key'

wk.register({
    ["<leader>n"] = { "<cmd>NvimTreeToggle<cr>", "Toggle NvimTree" }
    }
)
