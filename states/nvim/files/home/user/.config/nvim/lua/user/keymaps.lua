local opts = { noremap = true, silent = true }

local term_opts = { silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Normal --
-- Better window navigation
keymap("n", "<c-h>", "<c-w>h", opts) -- Move to left window
keymap("n", "<c-j>", "<c-w>j", opts) -- Move to right window
keymap("n", "<c-k>", "<c-w>k", opts) -- Move to top window
keymap("n", "<c-l>", "<c-w>l", opts) -- Move to bottom window

-- Resize with arrows
keymap("n", "<c-up>", ":resize -2<cr>", opts)
keymap("n", "<c-down>", ":resize +2<cr>", opts)
keymap("n", "<c-left>", ":vertical resize -2<cr>", opts)
keymap("n", "<c-right>", ":vertical resize +2<cr>", opts)

-- Navigate buffers
keymap("n", "<s-l>", "<cmd>BufferNext<cr>", opts)
-- keymap("n", "<m-tab>", "<cmd>BufferNext<cr>", opts)
keymap("n", "<s-h>", "<cmd>BufferPrevious<cr>", opts)
-- keymap("n", "<s-m-tab>", "<cmd>BufferPrevious<cr>", opts)

-- keymap("n", "<s-l>", ":bnext<cr>", opts)
keymap("n", "<m-tab>", ":bnext<cr>", opts)
-- keymap("n", "<s-h>", ":bprevious<cr>", opts)
keymap("n", "<s-m-tab>", ":bprevious<cr>", opts)

-- Move text up and down
keymap("n", "<a-j>", "<esc>:m .+1<cr>==gi", opts)
keymap("n", "<a-k>", "<esc>:m .-2<cr>==gi", opts)

-- Swap : and ;
-- keymap("n", ";", ":", opts)
-- keymap("n", ":", ";", opts)
vim.cmd([[nnoremap ; :]])
vim.cmd([[nnoremap : ;]])


-- Jump to 1-st char
keymap("n", "<c-0>", "^", opts)

-- Insert --
-- Press jj fast to enter
keymap("i", "jj", "<esc>", opts)
keymap("i", "kk", "<esc>", opts)
keymap("i", "jk", "<esc>", opts)
keymap("i", "kj", "<esc>", opts)

-- Visual --
-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- Move text up and down
keymap("v", "<a-j>", ":m .+1<cr>==", opts)
keymap("v", "<a-k>", ":m .-2<cr>==", opts)
keymap("v", "p", '"_dP', opts)

-- Visual Block --
-- Move text up and down
keymap("x", "J", ":move '>+1<cr>gv-gv", opts)
keymap("x", "K", ":move '<-2<cr>gv-gv", opts)
keymap("x", "<a-j>", ":move '>+1<cr>gv-gv", opts)
keymap("x", "<a-k>", ":move '<-2<cr>gv-gv", opts)

-- Terminal --
-- Better terminal navigation
keymap("t", "<c-h>", "<c-\\><c-n><c-w>h", term_opts)
keymap("t", "<c-j>", "<c-\\><c-n><c-w>j", term_opts)
keymap("t", "<c-k>", "<c-\\><c-n><c-w>k", term_opts)
keymap("t", "<c-l>", "<c-\\><c-n><c-w>l", term_opts)

-- NvimTree
--X keymap("n", "<leader>e", ":NvimTreeToggle<cr>", opts)

-- Telescope
-- keymap("n", "<leader>f", "<cmd>Telescope find_files<cr>", opts)
--X keymap("n", "<leader>f", "<cmd>lua require'telescope.builtin'.find_files(require('telescope.themes').get_dropdown({ previewer = false }))<cr>", opts)
--X keymap("n", "<c-t>", "<cmd>Telescope live_grep<cr>", opts)

--X keymap("n", "<leader>o", ":Format<cr>", opts)

