local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  vim.notify("Failed to load packer")
  -- print("Failed to load packer")
  return
end

-- Have packer use a popup window
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
}

-- Install your plugins here
return packer.startup(function(use)
  -- Plugin Manager
  use {"wbthomason/packer.nvim", commit = ""}              -- Have packer manage itself

  -- Lua Development
  use {"nvim-lua/popup.nvim", commit = ""}                 -- An implementation of the Popup API from vim in Neovim
  use {"nvim-lua/plenary.nvim", commit = ""}               -- Useful lua functions used by lots of plugins
  -- use {"christianchiarulli/lua-dev.nvim", commit = ""}
  use {"kkharji/sqlite.lua", commit = ""}

  -- Editing Support
  use {"windwp/nvim-autopairs", commit = ""}               -- Autopairs, integrates with both cmp and treesitter
  use {"numToStr/Comment.nvim", commit = ""}               -- Easily comment 
  use {"lukas-reineke/indent-blankline.nvim", commit = ""} -- Add indent guides 
  use {"kylechui/nvim-surround", commit = ""}              -- Add surround for braces and quotes and anything else
  -- use {"AckslD/nvim-neoclip.lua", commit = ""}             -- Add clipboard management

  -- File Explorer
  use {"kyazdani42/nvim-tree.lua", commit = ""}            -- Powerful file explorer
  use {"kyazdani42/nvim-web-devicons", commit = ""}        -- Adds pretty icons to nvim-tree

  -- Tabline and Statusline
  -- use {"akinsho/bufferline.nvim", commit = ""}          -- Show line of open buffers similar to tabs
  use {"romgrk/barbar.nvim", commit = ""}                  -- Show line of open buffers similar to tabs
  use {"nvim-lualine/lualine.nvim", commit = ""}           -- Show git status and more info in nvim status line
  
  -- LSP
  use {"neovim/nvim-lspconfig", commit = ""}               -- enable LSP
  use {"williamboman/nvim-lsp-installer", commit = ""}     -- simple to use language server installer
  use {"tamago324/nlsp-settings.nvim", commit = ""}        -- language server settings defined in json for
  use {"jose-elias-alvarez/null-ls.nvim", commit = ""}     -- for formatters and linters

  -- Utilities
  use {"goolord/alpha-nvim", commit = ""}                  -- Greeting window
  use {"moll/vim-bbye", commit = ""}                       -- Close buffer w/o closing nvim
  use {"lewis6991/impatient.nvim", commit = ""}            -- Speed loading of lua modules to improve nvim startup time

  -- Project
  --   use {"ahmedkhalf/project.nvim", commit = ""}             -- Integrate projects to Treesitter project

  -- Session

  -- Keybindings for leading key
  use {"folke/which-key.nvim", commit = ""}                -- Provide selection menu for leading key space

  -- Colorschemes
  -- use {"lunarvim/colorschemes", commit = ""}            -- A bunch of colorschemes you can try out
  use {"lunarvim/darkplus.nvim", commit = ""}

  -- Completion
  -- use {"christianchiarulli/nvim-cmp", commit = ""}      -- The completion plugin
  use {"hrsh7th/nvim-cmp", commit = ""}                    -- The completion plugin
  use {"hrsh7th/cmp-buffer", commit = ""}                  -- buffer completions
  use {"hrsh7th/cmp-path", commit = ""}                    -- path completions
  use {"hrsh7th/cmp-cmdline", commit = ""}                 -- cmdline completions
  use {"saadparwaiz1/cmp_luasnip", commit = ""}            -- snippet completions
  use {"hrsh7th/cmp-nvim-lsp", commit = ""}
  -- use {"hrsh7th/cmp-emoji", commit = ""}
  -- use {"hrsh7th/cmp-nvim-lua", commit = ""}

  -- Snippets
  use {"L3MON4D3/LuaSnip", commit = ""}                    --snippet engine
  use {"rafamadriz/friendly-snippets", commit = ""}        -- a bunch of snippets to use

  -- Telescope
  use {"nvim-telescope/telescope.nvim", commit = ""}
  use {"nvim-telescope/telescope-media-files.nvim", commit = ""}

  -- Treesitter
  use {
    "nvim-treesitter/nvim-treesitter",
    commit = "",
    run = ":TSUpdate",
  }
  use {"JoosepAlviste/nvim-ts-context-commentstring", commit = ""}

  -- Git
  use {"lewis6991/gitsigns.nvim", commit = ""}
  -- use {"f-person/git-blame.nvim", commit = ""}
  -- use {"ruifm/gitlinker.nvim", commit = ""}
  -- use {"mattn/vim-gist", commit = ""}
  -- use {"mattn/webapi-vim", commit = ""}

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
