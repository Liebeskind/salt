-------------
-- LuaSnip --
-------------
local luasnip = require("luasnip")
local types = require("luasnip.util.types")
luasnip.setup({
    history = true,
    update_events = "TextChanged,TextChangedI",
    delete_check_events = "TextChanged",
    ext_opts = {
        [types.choiceNode] = {
            active = {
--                virt_text = { { "choiceNode", "Comment" } },
                virt_text = {{"●", "GruvboxOrange"}}
            },
        },
        [types.insertNode] =  {
            active = {
                virt_text = {{"●", "GruvboxBlue"}}
            }
        }
    },
    ext_base_prio = 300,
    ext_prio_increase = 1,
    enable_autosnippets = true,
    store_selection_keys = "<Tab>",
    ft_func = function()
        return vim.split(vim.bo.filetype, ".", true)
    end,
})

require("luasnip.loaders.from_lua").load({paths = "~/.config/nvim/luasnips/"})

vim.api.nvim_create_user_command(
    "LuaSnipEdit",
    function()
        require("luasnip.loaders").edit_snippet_files()
    end,
    { nargs = 0 }
)

vim.keymap.set("n", "<leader><leader>s", "<cmd>source ~/.config/nvim/lua/snip.lua<CR>")
