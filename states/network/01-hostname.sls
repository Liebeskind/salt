{% set hostname = grains.id %}

{{ tplfile }}> Create /etc/hosts:
  file.managed:
    - name: /etc/hosts
    - source: salt://{{ slspath }}/files/etc/hosts
    - template: jinja
    - defaults:
        hostname: {{ hostname }}
    - creates: /etc/hosts

