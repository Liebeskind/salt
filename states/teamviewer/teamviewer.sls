{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
  cmd.run:
    - name: yay -S {{ slspath }} --noconfirm --answerclean None --answerdiff None --answeredit None --noprovides
    - unless: test -f /usr/bin/{{ slspath }}

{{ tplfile }}> Enable service {{ slspath }}:
  service.enabled:
  - name: teamviewerd.service

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  file.recurse:
    - name: /home/{{ user }}/.config/{{ slspath }}
    - source: salt://{{ slspath }}/files/home/user/.config/{{ slspath }}
    - makedirs: True
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - {{ tplfile }}> Install {{ slspath }} package
      - user: {{ user }}


