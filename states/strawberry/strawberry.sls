{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
  pkg.installed:
    - name: {{ slspath }}

# {{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
#   file.recurse:
#     - name: /home/{{ user }}/.config/{{ slspath }}
#     - source: salt://{{ slspath }}/files/home/user/.config/{{ slspath }}
#     - makedirs: True
#     - user: {{ user }}
#     - group: {{ user }}
#     - require:
#       - {{ tplfile }}> Install {{ slspath }} package
#       - user: {{ user }}

#+ {{ tplfile }}> Restore {{ slspath }} config for user {{ user }}:
#+   cmd.run:
#+     - name: rsync -a /backup/restore-user/.config/{{ slspath }} /home/{{ user }}/.config/
#+     - unless: test -d /home/{{ user }}/.config/{{ slspath }}
#+     - require:
#+       - {{ tplfile }}> Install {{ slspath }} package

#+ {{ tplfile }}> Restore {{ slspath }} data for user {{ user }}:
#+   cmd.run:
#+     - name: rsync -a /backup/restore-user/.local/share/{{ slspath }}
#+     - onlyif: test -d /backup/restore-user/.local/share/{{ slspath }}
#+     - unless: test -d /home/{{ user }}/.local/share/{{ slspath }}
#+     - require:
#+       - {{ tplfile }}> Install {{ slspath }} package


#  file.recurse:
#    - name: /root/.config/strawberry
#    - source: salt://{{ slspath }}/files/.config/strawberry
#    - makedirs: True
#    - require:
#      - {{ tplfile }}> Install strawberry package
