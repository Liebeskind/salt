{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
  cmd.run:
    - name: yay -S {{ slspath }} --answerclean None --answerdiff None --answeredit None
    - runas: {{ user }}
    - unless: test -f /usr/bin/{{ slspath }}

