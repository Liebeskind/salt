{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
  pkg.installed:
    - name: {{ slspath }}

#+ {{ tplfile }}> Restore {{ slspath }} data for user {{ user }}:
#+   cmd.run:
#+     - name: rsync -a /backup/restore-user/.local/share/{{ slspath }}
#+     - onlyif: test -d /backup/restore-user/.local/share/{{ slspath }}
#+     - unless: test -d /home/{{ user }}/.local/share/{{ slspath }}
#+     - require:
#+       - {{ tplfile }}> Install {{ slspath }} package
