{{ tplfile }}> Install required addtional packages:
  pkg.installed:
  - pkgs:
    - python-jmespath

{{ tplfile }}> Configure saltstack:
  file.recurse:
    - name: /etc/salt
    - source: salt://{{ slspath }}/files/etc/salt
    - makedirs: True

{{ tplfile }}> Enable saltstack minion service:
  service.running:
    - name: salt-minion
    - enable: True
    - watch:
       - file: {{ tplfile }}> Configure saltstack

#-- {{ tplfile }}> Add hack to avoid warning about Setuptools is replacing distutils:
#--   file.replace
#--     - name: /usr/lib/python3.10/site-packages/salt/__init__.py 
#--     - backup: false
#--     - pattern: |
#--         ^def __define_global_system_encoding_variable__.*:$ 
#--     - repl: |
#--         # NOTE: The fix is taken from here: https://github.com/s0undt3ch/salt/blob/1b7fac1599a50c970fdef49abca7151cb11bdcd0/salt/__init__.py 
#--         #       This fix is inserted via git/talentix/salt-bootstrap/install.sh 
#--         #       Filter the backports package UserWarning about being re-imported 
#--         warnings.filterwarnings( 
#--             "ignore", 
#--             message="Setuptools is replacing distutils.", 
#--             category=UserWarning, 
#--             module="_distutils_hack", 
#--         ) 
#-- 
#-- 
#--        \1 
#--     - bufsize: file


