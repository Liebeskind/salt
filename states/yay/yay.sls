{% set user    = grains.user                  %}
{% set src     = '/home/' ~ user ~ '/software/aur' %}
{% set src_yay = src ~ '/yay'                 %}

# {{ tplfile }}> Download and install yay AUR installer from git:
#   cmd.run:
#     - name: rm -rf yay & git clone https://aur.archlinux.org/yay.git & cd yay & makepkg -si
#     - runas: 1000
#     - cwd: /home/{{ user }}
# # #    - creates:


{{ tplfile }}> Create sources dir:
  file.directory:
    - name: {{ src }}
    - user: {{ user }}
    - group: {{ user }}
#    - unless: which yay

{{ tplfile }}> Delete yay source dir:
  file.absent:
    - name: {{ src_yay }}
    - unless: which yay

#    - runas: 1000
#    - cwd: /home/uri
#    - onlyif: test -d /home/uri/yay

{{ tplfile }}> Clone yay:
  cmd.run:
    - name: git clone https://aur.archlinux.org/yay.git
    - runas: {{ user }}
    - cwd: {{ src }}
#    - require:
#      - {{ tplfile }}> Delete yay source dir
    - unless: test -d {{ src_yay }}
    - require:
      - pkg: sudo

{{ tplfile }}> Make and install package:
  cmd.run:
    - name: makepkg -si
    - runas: {{ user }}
    - cwd: {{ src_yay }}
    - require:
      - {{ tplfile }}> Clone yay
    - onlyif: test -f '{{ src_yay }}/PKGBUILD'
    - unless: which yay


