{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
  pkg.installed:
    - name: {{ slspath }}

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  file.recurse:
    - name: /home/{{ user }}/.config/{{ slspath }}
    - source: salt://{{ slspath }}/files/home/user/.config/{{ slspath }}
    - makedirs: True
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - {{ tplfile }}> Install {{ slspath }} package
      - user: {{ user }}

