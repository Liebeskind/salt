{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
  cmd.run:
    - name: yay -S {{ slspath }} --answerclean None --answerdiff None --answeredit None
    - unless: test -f /usr/bin/{{ slspath }}

#+   pkg.installed:
#+     - pkgs:
#+       - {{ slspath }}

#+ {{ tplfile }}> Restore {{ slspath }} data for user {{ user }}:
#+   cmd.run:
#+     - name: rsync -a /backup/restore-user/.local/share/{{ slspath }}
#+     - onlyif: test -d /backup/restore-user/.local/share/{{ slspath }}
#+     - unless: test -d /home/{{ user }}/.local/share/{{ slspath }}
#+     - require:
#+       - {{ tplfile }}> Install {{ slspath }} package

