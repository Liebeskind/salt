# Automatic titles
# function fish_title
#     echo (status current-command) ' '
# end

# Start the ssh agent.
agent_fish

# Aliases
alias vim nvim

set -g -x PATH $HOME/.local/bin $HOME/.scripts $HOME/.yarn/bin $PATH
set -g -x LD_LIBRARY_PATH $HOME/.local/lib $LD_LIBRARY_PATH

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
eval $HOME/.local/opt/miniconda3/bin/conda "shell.fish" "hook" $argv | source
# <<< conda initialize <<<

# opam configuration
source /home/joram/.opam/opam-init/init.fish > /dev/null 2> /dev/null; or true

source $XDG_CONFIG_HOME/fish/colors/nightfox_nightfox.fish

