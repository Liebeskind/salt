function agent_fish -d "start new ssh agent"
    if test -z "$SSH_ENV"
        set -xg SSH_ENV $HOME/.ssh/agent-env
    end

    if not __ssh_agent_running
        ssh-agent -c | sed 's/^echo/#echo/' > $SSH_ENV
        chmod 600 $SSH_ENV
        source $SSH_ENV > /dev/null
    end
end

