function set_var -a var val
    for v in $$var
        if test "$v" = "$val"
            return
        end
    end
    set -g -x {$var} "$val" $$var
end

function scewo_apps_env
    set linux_applications_path "/home/joram/Programming/SCEWO/linux-applications"
    set_var LD_LIBRARY_PATH "$linux_applications_path/sysroot/lib"
    set_var PKG_CONFIG_PATH "$linux_applications_path/sysroot/lib/pkgconfig"
    set_var PATH "$linux_applications_path/sysroot/bin"
end

