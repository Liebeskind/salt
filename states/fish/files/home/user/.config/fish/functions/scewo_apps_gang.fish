function scewo_apps_gang
    scewo_apps_env
    mkdir -p /tmp/scewo_logs/
    kv-server -s /tmp/kv-server.sock -l "" > /tmp/scewo_logs/kv-server.log 2>&1 &
    sleep 0.1
    rtoscewo-simulator -l "" -s /tmp/kv-server.sock > /tmp/scewo_logs/rtoscewo-simulator.log 2>&1 &
    kv-tcp-proxy -l "" -I lo -s /tmp/kv-server.sock -k ~/Programming/SCEWO/kv-tcp-proxy-keys/key.pem -c ~/Programming/SCEWO/kv-tcp-proxy-keys/cert.pem -token-db /tmp/tokens.config > /tmp/scewo_logs/kv-tcp-proxy.log 2>&1 &
    back-camera -l "" -s /tmp/kv-server.sock > /tmp/scewo_logs/back-camera.log 2>&1 &
    sleep 0.1
    kv-client -s 190,uint8,5 > /dev/null 2>&1
    kv-client -s 305,uint8,1 > /dev/null 2>&1
end

