function eo
    set ETH_OPEN_TARGET (find $HOME/Documents/Studium/ETH/ | grep -vP '\.stack|\.git|\.log|\.aux|\.fls|\.synctex.gz|\.fdb_latexmk|\.stfolder|\.DS_Store|\.class|\.jar|\.project|\.metadata|\.settings|__MACOSX|\.recommenders|\.idea|\.stversions' | fzf)

    if test ! $status -eq 0
        return 0
    end
    if test -f $ETH_OPEN_TARGET
        cd (dirname $ETH_OPEN_TARGET)
        mimeo $ETH_OPEN_TARGET
    else if test -d $ETH_OPEN_TARGET
        cd $ETH_OPEN_TARGET
    end
end

