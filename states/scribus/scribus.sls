{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
  pkg.installed:
    - name: {{ slspath }}

#{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
#  file.recurse:
#    - name: /home/{{ user }}/.config/{{ slspath }}
#    - source: salt://{{ slspath }}/files/home/user/.config/{{ slspath }}
#    - makedirs: True
#    - user: {{ user }}
#    - group: {{ user }}
#    - require:
#      - {{ tplfile }}> Install {{ slspath }} package
#      - user: {{ user }}


#+ {{ tplfile }}> Restore {{ slspath }} data for user {{ user }}:
#+   cmd.run:
#+     - name: rsync -a /backup/restore-user/.local/share/{{ slspath }}
#+     - onlyif: test -d /backup/restore-user/.local/share/{{ slspath }}
#+     - unless: test -d /home/{{ user }}/.local/share/{{ slspath }}
#+     - require:
#+       - {{ tplfile }}> Install {{ slspath }} package


#  file.recurse:
#    - name: /root/.config/scribus
#    - source: salt://{{ slspath }}/files/.config/scribus
#    - makedirs: True
#    - require:
#      - {{ tplfile }}> Install scribus package
