{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} packages:
  pkg.installed:
    - name: {{ slspath }}

{{ tplfile }}> Configure {{ slspath }}:
  file.recurse:
    - name: /home/{{ user }}/.config/{{ slspath }}
    - source: salt://{{ slspath }}/files/home/user/.config/{{ slspath }}
    - makedires: True
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - {{ tplfile }}> Install {{ slspath }} packages
      - user: {{ user }}


{{ tplfile }}> Configure {{ slspath }} for user root:
  file.recurse:
    - name: /root/.config/{{ slspath }}
    - source: salt://{{ slspath }}/files/root/.config/{{ slspath }}
    - makedirs: True
    - require:
      - {{ tplfile }}> Install {{ slspath }} packages

