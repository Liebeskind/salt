{% set user = grains.user %}

{{ tplfile }}> Install {{ slspath }} package:
cmd.run:
  - name: yay -S {{ slspath }} --answerclean None --answerdiff None --answeredit None
  - runas: {{ user }}
  - unless: test -f /usr/lib/systemd/system/logid.service

{{ tplfile }}> Enable and start logid.service:
  service.running:
    - name: logid.service
    - enable: True
    - require:
      - {{ tplfile }}> Install {{ slspath }} package

