{{ tplfile }}> Install font packages:
  pkg.installed:
    - pkgs:
      - gnu-free-fonts
      - gsfonts
      - ttf-opensans
      - ttf-ubuntu-font-family
#      - ttf-iosevka
#      - ttf-iosevka-nerd
      - adobe-source-code-pro-fonts
      - noto-fonts
      - terminus-font
      - opendesktop-fonts
      - ttf-bitstream-vera
      - ttf-croscore
      - ttf-dejavu
      - ttf-droid
      - ttf-font-awesome
      - ttf-ibm-plex
      - ttf-inconsolata
      - ttf-joypixels
      - ttf-liberation
      - ttf-linux-libertine
      - ttf-roboto
      - ttf-roboto-mono
      - ttf-fira-mono

